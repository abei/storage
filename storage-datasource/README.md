Instruction of phase-1
  1. For the first phase, no third party data source tool is imported, just JDBC.
  2. Abandon DataSourceAutoConfiguration of springboot.
      @SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
  3. Import org.projectlombok, because I used its annotation - @Data



