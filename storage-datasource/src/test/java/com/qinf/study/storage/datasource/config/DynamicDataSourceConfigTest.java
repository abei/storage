package com.qinf.study.storage.datasource.config;

import com.qinf.study.storage.datasource.Application;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

/**
 * Created by qinf on 2019-04-11.
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DynamicDataSourceConfigTest {

    @Autowired
    private DynamicDataSourceAutoConfig dynamicDataSourceConfig;

    @Test
    public void testLoadDynamicDataSourceConfig() {
        DataSource defaultDataSource = dynamicDataSourceConfig.defaultDataSource();
        DataSource dynamicDataSource = dynamicDataSourceConfig.dynamicDataSource();
        log.info("The configured default data source config is : {}", defaultDataSource);
        log.info("The configured dynamic data source config is : {}", dynamicDataSource);
    }
}
