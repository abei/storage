package com.qinf.study.storage.datasource.config;

import com.qinf.study.storage.datasource.Application;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by qinf on 2019-04-08.
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DataSourceConfigTest {

    @Autowired
    private DataSourceConfig dataSourceConfig;

    @Test
    public void testLoadDataSourceConfig() {
        log.info("The loaded data source config is : {}", dataSourceConfig.getGroups());
    }
}
