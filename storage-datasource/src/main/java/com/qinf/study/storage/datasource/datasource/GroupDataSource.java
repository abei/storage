package com.qinf.study.storage.datasource.datasource;

import lombok.Data;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qinf on 2019-04-14.
 */
@Data
public class GroupDataSource {
    private String groupName;
    private WrappedDataSource master;
    private List<WrappedDataSource> slaves = new ArrayList<>();

    @Data
    public static class WrappedDataSource {
        private String name;
        private DataSource dataSource;
    }
}
