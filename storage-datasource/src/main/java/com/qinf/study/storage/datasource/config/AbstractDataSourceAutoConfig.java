package com.qinf.study.storage.datasource.config;

import com.qinf.study.storage.datasource.datasource.GroupDataSource;
import com.qinf.study.storage.datasource.exception.InitDataSourceException;
import io.shardingsphere.core.api.MasterSlaveDataSourceFactory;
import io.shardingsphere.core.api.config.MasterSlaveRuleConfiguration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by qinf on 2019-04-15.
 */
public abstract class AbstractDataSourceAutoConfig {

    protected DataSource createDefaultDataSource(DataSourceConfig dataSourceConfig) {
        DataSourceConfig.Group group = dataSourceConfig.getGroups().stream()
                .filter(grp -> Objects.equals(grp.getName(), "default"))
                .findAny().orElseThrow(() -> new InitDataSourceException("Default database is null."));
        return this.createConcreteDataSource(group.getMaster());
    }

    /**
     * The method does two things, one is convert data source config into
     * concrete data source; another thing is create master slave data source
     * by sharding-jdbc third-party jar. The generated master-slave data source
     * is the base of read-write separation.
     *
     * @Param groupConf data source configuration
     */
    protected Map<Object, Object> createTargetDataSources(DataSourceConfig dataSourceConfig) {
        List<GroupDataSource> groupDataSources = this.createGroupDataSource(dataSourceConfig.getGroups());
        Map<Object, Object> targetDataSources = groupDataSources.stream().collect(
                Collectors.toMap(
                        groupDataSource -> groupDataSource.getGroupName(),
                        groupDataSource -> this.createMasterSlaveDataSource(groupDataSource)));
        return targetDataSources;
    }

    private List<GroupDataSource> createGroupDataSource(List<DataSourceConfig.Group> groupConf) {
        List<GroupDataSource> groupDataSources = groupConf.stream()
                .map(group -> {
                    GroupDataSource groupDataSource = new GroupDataSource();
                    String groupName = group.getName();
                    GroupDataSource.WrappedDataSource wrappedMaster = new GroupDataSource.WrappedDataSource();
                    String masterDataSourceName = group.getMaster().getName();
                    DataSource masterDataSource = this.createConcreteDataSource(group.getMaster());
                    wrappedMaster.setName(masterDataSourceName);
                    wrappedMaster.setDataSource(masterDataSource);
                    List<GroupDataSource.WrappedDataSource> wrappedSlaves = group.getSlaves().stream()
                            .map(slave -> {
                                GroupDataSource.WrappedDataSource wrappedSlave = new GroupDataSource.WrappedDataSource();
                                String slaveDataSourceName = slave.getName();
                                DataSource slaveDataSource = this.createConcreteDataSource(slave);
                                wrappedSlave.setName(slaveDataSourceName);
                                wrappedSlave.setDataSource(slaveDataSource);
                                return wrappedSlave; })
                            .collect(Collectors.toList());
                    groupDataSource.setGroupName(groupName);
                    groupDataSource.setMaster(wrappedMaster);
                    groupDataSource.setSlaves(wrappedSlaves);
                    return groupDataSource; })
                .collect(Collectors.toList());
        return groupDataSources;
    }

    private DataSource createMasterSlaveDataSource(GroupDataSource groupDataSource) {
        try {
            Map<String, DataSource> dataSourceMap = new HashMap<>();
            dataSourceMap.put(groupDataSource.getMaster().getName(), groupDataSource.getMaster().getDataSource());
            dataSourceMap.putAll(groupDataSource.getSlaves().stream().collect(Collectors.toMap(slave -> slave.getName(), slave -> slave.getDataSource())));
            MasterSlaveRuleConfiguration masterSlaveRuleConfig = this.initMasterSlaveRule(groupDataSource);
            return MasterSlaveDataSourceFactory.createDataSource(dataSourceMap, masterSlaveRuleConfig, new ConcurrentHashMap<>());
        } catch (SQLException e) {
            throw new InitDataSourceException("Failed to create master salve data source.", e);
        }
    }

    /**
     * The below codes implement two routing rules by sharding-jdbc:
     * 1. Master & slaves routing rule.
     * 2. Slaves routing rule. Here we don't implement slaves routing rule(algorithm), so the default
     *    routing rule(ROUND_ROBIN) will be used.
     */
    private MasterSlaveRuleConfiguration initMasterSlaveRule(GroupDataSource groupDataSource) {
        String groupName = groupDataSource.getGroupName();
        String masterName = groupDataSource.getMaster().getName();
        List<String> slaveNames = groupDataSource.getSlaves().stream()
                .map(GroupDataSource.WrappedDataSource::getName)
                .collect(Collectors.toList());
        MasterSlaveRuleConfiguration ruleConfig = new MasterSlaveRuleConfiguration(groupName, masterName, slaveNames);
        return ruleConfig;
    }

    protected abstract DataSource createConcreteDataSource(DataSourceConfig.DataSource dataSourceConf);
}
