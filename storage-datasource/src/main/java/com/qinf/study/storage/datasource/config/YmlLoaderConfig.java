package com.qinf.study.storage.datasource.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import java.util.Optional;

/**
 * Created by qinf on 2019-04-08.
 *
 * Load data source config into memory.
 * NOTE : Constructor >> setApplicationContext >> @Bean >> @Autowired >> @PostConstruct
 */
@Slf4j
@Configuration
public class YmlLoaderConfig implements ApplicationContextAware {
    private String activeProfile;

    public YmlLoaderConfig() {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.activeProfile = Optional.ofNullable(applicationContext)
                .map(ApplicationContext::getEnvironment)
                .map(Environment::getActiveProfiles)
                .map(profiles -> profiles[0]).orElse("");
    }

    @Bean
    public PropertySourcesPlaceholderConfigurer properties() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        YamlPropertiesFactoryBean ymlFactory = new YamlPropertiesFactoryBean();
        log.info("Begin to load data source config : {}.", "database/mysql-" + this.activeProfile + ".yml");
        //ymlFactory.setResources(new FileSystemResource("config.yml"));
        ymlFactory.setResources(new ClassPathResource("database/mysql-" + this.activeProfile + ".yml"));
        log.info("Load data source config successfully.");
        configurer.setProperties(ymlFactory.getObject());
        return configurer;
    }
}
