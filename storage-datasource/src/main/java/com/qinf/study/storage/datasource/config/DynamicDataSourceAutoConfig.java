package com.qinf.study.storage.datasource.config;

import com.qinf.study.storage.datasource.datasource.RoutingDynamicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

/**
 * Created by qinf on 2019-04-09.
 */
@Configuration
@ConditionalOnClass(DataSource.class)
@Import(DataSourceConfig.class)
public class DynamicDataSourceAutoConfig extends AbstractDataSourceAutoConfig {

    @Autowired
    private DataSourceConfig dataSourceConfig;

    @Bean
    public DataSource defaultDataSource() {
        return this.createDefaultDataSource(dataSourceConfig);
    }

    /**
     * @Primary the annotation make the dynamic data source
     * injected into SqlSessionFactory.
     */
    @Primary
    @Bean
    public DataSource dynamicDataSource() {
        RoutingDynamicDataSource dynamicDataSourceRouting = new RoutingDynamicDataSource();
        dynamicDataSourceRouting.setDefaultTargetDataSource(defaultDataSource());
        dynamicDataSourceRouting.setTargetDataSources(this.createTargetDataSources(dataSourceConfig));
        return dynamicDataSourceRouting;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dynamicDataSource());
    }

    /**
     * Create concrete data source. The real data source can be created by different
     * third party data-source jar. For example, just the jdbc, or druid, c3p0 etc.
     * Here may need use template design pattern to make the config more flexible.
     */
    @Override
    protected DataSource createConcreteDataSource(DataSourceConfig.DataSource dataSourceConf) {
        DriverManagerDataSource jdbcDataSource = new DriverManagerDataSource();
        jdbcDataSource.setDriverClassName(dataSourceConf.getDriver());
        jdbcDataSource.setUrl(dataSourceConf.getUrl());
        jdbcDataSource.setUsername(dataSourceConf.getUsername());
        jdbcDataSource.setPassword(dataSourceConf.getPassword());
        return jdbcDataSource;
    }
}