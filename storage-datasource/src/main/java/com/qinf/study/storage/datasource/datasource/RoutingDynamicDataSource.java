package com.qinf.study.storage.datasource.datasource;

import com.qinf.study.storage.datasource.DynamicDataSourceContext;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * Created by qinf on 2019-04-11.
 */
public class RoutingDynamicDataSource extends AbstractRoutingDataSource {
    /**
     * The reason of you can get the specific data source is:<br>
     * 1. you have put all data sources into a map belonged to AbstractRoutingDataSource.class<br>
     *    by setTargetDataSources(allDataSources)<br>
     * 2. you have set the key of specific data source into ThreadLocal before use it.<br>
     * 3. then, here, you can get the key.<br>
     * 4. last, AbstractRoutingDataSource will help you get the specific data source.<br>
     */
    @Override
    protected Object determineCurrentLookupKey() {
        try {
            return DynamicDataSourceContext.getCurrentDataSource();
        } finally {
            DynamicDataSourceContext.clear();
        }
    }
}
