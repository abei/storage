package com.qinf.study.storage.datasource;

import io.shardingsphere.core.api.HintManager;
import io.shardingsphere.core.hint.HintManagerHolder;

/**
 * Created by qinf on 2019-04-11.
 */
public class DynamicDataSourceContext {

    private static final ThreadLocal<String> DATA_SOURCE_CACHE = new ThreadLocal();

    public static void clear() {
        DATA_SOURCE_CACHE.remove();
    }

    public static String getCurrentDataSource() {
        return DATA_SOURCE_CACHE.get();
    }

    public static void setCurrentDataSource(String dataSource) {
        DATA_SOURCE_CACHE.set(dataSource);
    }

    public static void setMasterRoute(boolean isMasterRoute) {
        if (isMasterRoute) {
            HintManager hintManager = HintManager.getInstance();
            hintManager.setMasterRouteOnly();
        } else {
            HintManagerHolder.clear();
        }
    }
}
