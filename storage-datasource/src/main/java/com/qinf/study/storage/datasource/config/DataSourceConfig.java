package com.qinf.study.storage.datasource.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qinf on 2019-04-08.
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "mysql")
public class DataSourceConfig {
    private List<Group> groups;

    @Data
    public static class Group {
        private String name;
        private String selectSlavePolicy;
        private DataSource master;
        private List<DataSource> slaves = new ArrayList();
    }

    @Data
    public static class DataSource {
        private String name;
        private String driver;
        private String url;
        private String username;
        private String password;
    }
}
