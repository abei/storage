package com.qinf.study.storage.datasource.exception;

/**
 * Created by qinf on 2019-04-11.
 */
public class InitDataSourceException extends RuntimeException {
    public InitDataSourceException() {
        super();
    }

    public InitDataSourceException(String s) {
        super(s);
    }

    public InitDataSourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public InitDataSourceException(Throwable cause) {
        super(cause);
    }
}
