package com.qinf.study.storage.plugin.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by qinf on 2019-04-21.
 *
 * Used to tag specific data source group.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface DataSourceGroup {
    String groupName() default "default";
}
