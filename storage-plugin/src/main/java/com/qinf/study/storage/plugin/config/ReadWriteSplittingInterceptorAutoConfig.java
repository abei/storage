package com.qinf.study.storage.plugin.config;

import com.qinf.study.storage.datasource.config.DynamicDataSourceAutoConfig;
import com.qinf.study.storage.plugin.interceptor.ReadWriteSplittingInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by qinf on 2019-04-21.
 */
@Configuration
@ConditionalOnClass({SqlSessionFactory.class, SqlSessionFactoryBean.class})
@AutoConfigureAfter({DynamicDataSourceAutoConfig.class})
public class ReadWriteSplittingInterceptorAutoConfig {

    @Bean
    @ConditionalOnMissingBean
    public Interceptor readWriteSplittingInterceptor() {
        return new ReadWriteSplittingInterceptor();
    }
}
