package com.qinf.study.storage.plugin.interceptor;

import com.qinf.study.storage.datasource.DynamicDataSourceContext;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

/**
 * Created by qinf on 2019-04-21.
 * <p>
 * If you want to implement your own interceptor, Executor or StatementHandler,
 * one of them can be signatured. But, Executor has more functions, so here we
 * signature Executor.
 */
@Intercepts({
        @Signature(
                type = Executor.class,
                method = "update",
                args = {MappedStatement.class, Object.class}),
        @Signature(
                type = Executor.class,
                method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
public class ReadWriteSplittingInterceptor extends AbstractInterceptor {

    private MappedStatement mappedStatement;

    /**
     * The method is invoked from Plugin by dynamic proxy of JDK.
     * Some moments the debug scene may like following<br>
     * Plugin.invoke(Object proxy, Method method, Object[] args)<br>
     * proxy : proxy class of Executor
     * method : Executor.update(MappedStatement)
     * args : MappedStatement & parameter values
     * <p>
     * Invocation(Object target, Method method, Object[] args)
     * target : Executor
     * method : Executor.update(MappedStatement)
     * args : MappedStatement & parameter values
     */
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        Object[] args = invocation.getArgs();
        if (Objects.nonNull(args) && args.length > 0 && Objects.nonNull(args[0])) {
            this.mappedStatement = (MappedStatement) args[0];
            /**
             * 1. Dynamic datasource(group level).
             * At the same time, splitting read and write.
             */
            Class<?> mapperClass = (Class<?>) this.getMapperObject(LocalCache.CacheType.CLASS);
            DataSourceGroup dataSourceGroup = Objects.isNull(mapperClass) ? null : mapperClass.getAnnotation(DataSourceGroup.class);
            if (Objects.nonNull(dataSourceGroup))
                DynamicDataSourceContext.setCurrentDataSource(dataSourceGroup.groupName());
            else
                DynamicDataSourceContext.setCurrentDataSource(null);

            /**
             * 2. Force read-write operation route to master database(database level),
             * there are two reasons for this function:
             * 1). Master-slave synchronize delay : if there is a delay of replication
             * between master and slave, you can force read or write operation routing
             * to master.
             * 2). Unify transaction to only one datasource : if there are two database
             * operations, reading and writing, and included in one transaction in code,
             * but due to read-write splitting, reading and writing will come from different
             * data sources. Besides, one transaction cannot cover two different data sources
             * and slave database cannot be used to write, so, this time you should force
             * reading and writing route to master database.
             * NOTE : As for 2), can implement a plugin to intercept transaction, force
             * reading and writing route to master database.
             */
            MasterRoute masterRouteOnClass = Objects.isNull(mapperClass) ? null : mapperClass.getAnnotation(MasterRoute.class);
            Method mapperMethod = (Method) this.getMapperObject(LocalCache.CacheType.METHOD);
            MasterRoute masterRouteOnMethod = Objects.isNull(mapperMethod) ? null : mapperMethod.getAnnotation(MasterRoute.class);
            if (Objects.nonNull(masterRouteOnClass) || Objects.nonNull(masterRouteOnMethod))
                DynamicDataSourceContext.setMasterRoute(true);
            else
                DynamicDataSourceContext.setMasterRoute(false);
        }
        return invocation.proceed();
    }

    /**
     * The method is invoked by Interceptor chain of mybatis
     * (public Object pluginAll(Object target)).<br>
     * The method used to decide if need generate proxy class.
     * If a proxy class is generated, the intercept method above
     * will be invoked from invoke() method of InvocationHandler
     * of JDK by dynamic proxy mechanism.<br>
     * Here, we just want intercept Executor and do some additional
     * work, so, we just generate proxy class when target instanceof
     * Executor.<br>
     *
     * @param target the target can be StatementHandler, ResultSetHandler,
     *               ParameterHandler or Executor. It may be different as each
     *               invocation. Because, it works for whole interceptor chain.
     */
    @Override
    public Object plugin(Object target) {
        /**
         * In fact, the fragment of below code may be redundant, this is because
         * the method Plugin.wrap() can decide if need generate proxy class by the
         * target, but a little performance optimized can come from the below code by
         * reducing the count of invoking method Plugin.wrap().
         */
        return target instanceof Executor ? Plugin.wrap(target, this) : target;
    }

    @Override
    public void setProperties(Properties properties) {
    }

    /**
     * Get mapper class or method from mybatis framework.
     *
     * @param cacheType cache type
     */
    private Object getMapperObject(LocalCache.CacheType cacheType) {
        /**
         * The id is the sql statement id = mapper namespace + statement id in mapper.xml,
         * for instance:
         * <mapper namespace = "com.qinf.study.storage.plugin.interceptor.mapper"...
         * <insert id = "insertOrder" parameterType = ....
         * then the mappedStatementId = com.qinf.study.storage.plugin.interceptor.mapper.OrderMapper.insertOrder
         */
        String mappedStatementId = this.mappedStatement.getId();
        Object mapperObject = null;
        if (Objects.isNull(mappedStatementId) || mappedStatementId.isEmpty()) {
            return mapperObject;
        }
        String mapperClassFullName = mappedStatementId.substring(0, mappedStatementId.lastIndexOf("."));
        if (Objects.equals(LocalCache.CacheType.CLASS, cacheType)) {
            mapperObject = LocalCache.get(mapperClassFullName, cacheType);
            if (Objects.nonNull(mapperObject))
                return mapperObject;
            Collection<Class<?>> knownMapperClasses = this.mappedStatement
                    .getConfiguration().getMapperRegistry().getMappers();
            mapperObject = knownMapperClasses.stream()
                    .filter(clz -> Objects.equals(clz.getName(), mapperClassFullName))
                    .findAny().get();
            if (Objects.nonNull(mapperObject))
                LocalCache.put(mapperClassFullName, mapperObject);
        }
        if (Objects.equals(LocalCache.CacheType.METHOD, cacheType)) {
            String mapperMethodFullName = mappedStatementId;
            mapperObject = LocalCache.get(mapperMethodFullName, cacheType);
            if (Objects.nonNull(mapperObject))
                return mapperObject;
            Class mapperClass = LocalCache.get(mapperClassFullName, LocalCache.CacheType.CLASS);
            /**
             * As we know, jdk still not solved autoboxing when get method object by reflect with
             * the passed parameter type. In other words, if there is an overloading method in a
             * interface class, you have to specify accurate parameter information(i.e, parameter
             * type, parameter count) when get it by Class.getDeclaredMethod(String methodName, Class<?> parameterTypes).
             * For instance, int.class of a method parameter type is different with Integer.class.
             * But in mybatis context, we think it's impossible that there are two methods with
             * same name in one interface. So, here, we just get the desired mapper method by
             * method name.
             */
            Method[] mapperMethods = Objects.isNull(mapperClass) ? null : mapperClass.getDeclaredMethods();
            mapperObject = Objects.isNull(mapperMethods) ? null : Stream.of(mapperMethods)
                    .filter(mtd -> Objects.equals(mtd.getDeclaringClass().getName() + "." + mtd.getName(), mapperMethodFullName))
                    .findAny().get();
            if (Objects.nonNull(mapperObject))
                LocalCache.put(mapperMethodFullName, mapperObject);
        }
        return mapperObject;
    }

    /**
     * Local cache used to cache mapper class or mapper method.
     */
    private static class LocalCache {
        private static final ConcurrentHashMap<String, Method> methodsCache = new ConcurrentHashMap<>();
        private static final ConcurrentHashMap<String, Class<?>> classesCache = new ConcurrentHashMap<>();

        static <K, V> V put(K key, V value) {
            Objects.requireNonNull(key);
            Objects.requireNonNull(value);
            if (value instanceof Method) {
                return (V) methodsCache.putIfAbsent((String) key, (Method) value);
            } else if (value instanceof Class) {
                return (V) classesCache.putIfAbsent((String) key, (Class) value);
            } else {
                throw new IllegalArgumentException("Not supported cache type, just can cache method and class for now.");
            }
        }

        static <V> V get(String key, CacheType cacheType) {
            V value;
            switch (cacheType) {
                case METHOD:
                    value = (V) methodsCache.get(key);
                    break;
                case CLASS:
                    value = (V) classesCache.get(key);
                    break;
                default:
                    throw new IllegalArgumentException("Not supported got operation type, just can get method and class for now.");
            }
            return value;
        }

        enum CacheType {
            METHOD, CLASS;
        }
    }
}
