package com.qinf.study.storage.plugin.interceptor;

import org.apache.ibatis.plugin.Interceptor;

/**
 * Created by qinf on 2019-04-21.
 *
 * As interceptor of mybatis, one important point of view should be raised.
 * That is, if you customized a interceptor, the interceptor can work whole
 * interceptor chain, that means your interceptor will be invoked each time
 * when operate DB, whereas, you can decide which type operation of DB can
 * trigger(by dynamic proxy) your interceptor logic, and which just let it go.
 *
 * What action can be intercepted by Interceptor of mybatis as following : <p>
 * 1). StatementHandler (prepare, parameterize, batch, update, query)<p>
 * 2). ResultSetHandler (handleResultSets, handleOutputParameters)<p>
 * 3). ParameterHandler (getParameterObject, setParameters)<p>
 * 4). Executor (update, query, flushStatements, commit, rollback, getTransaction, close, isClosed)
 */
public abstract class AbstractInterceptor implements Interceptor {
}
