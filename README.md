# study-storage

This is a project for studying storage related, hope more and more 
learning experience can be pushed to this project.

About data source model, I will focus on:
* dynamic datasource(group level) -> different datasource groups
* sharding databases(database level) -> different databases -> master & slaves splitting -> read and write splitting(replication)
* sharding tables(table level) -> different tables
* transaction(statement level)

About redis model, I will focus on:
* encapsulate operation of redis
* distributed lock